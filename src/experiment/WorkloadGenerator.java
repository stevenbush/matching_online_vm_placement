package experiment;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;
import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by jiyuanshi on 6/10/15.
 */
public class WorkloadGenerator {
    public static void main(String[] args) throws IOException {

        CSVWriter vminfo_writer = new CSVWriter(new FileWriter("./vm_info"), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER);
        CSVWriter eventinfo_writer = new CSVWriter(new FileWriter("./event_info"), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER);
        ExponentialDistribution starttimeDistribution = new ExponentialDistribution(2);
        UniformIntegerDistribution runtimeDistribution = new UniformIntegerDistribution(60, 180);

        TreeMap<Integer, ArrayList<String[]>> treeMap = new TreeMap<Integer, ArrayList<String[]>>();

        System.out.println("Start to generate workload data");

        CSVReader workload_conf_reader = new CSVReader(new FileReader("workload_conf_t10_d3_uniform"));
        List<String[]> items_list = workload_conf_reader.readAll();
        workload_conf_reader.close();

        int number_of_type = Integer.valueOf(items_list.get(0)[0]);
        int number_of_dimensions = Integer.valueOf(items_list.get(1)[0]);

        // Generate the VM information
        int[] type_name_list = new int[number_of_type];
        int[] ratio_list = new int[number_of_type];

        for (int i = 0; i < number_of_type; i++) {
            type_name_list[i] = i;
            ratio_list[i] = Integer.valueOf(items_list.get(2)[i]);
        }

        int[][] type_conf_list = new int[number_of_type][number_of_dimensions];
        for (int i = 0; i < number_of_type; i++) {
            for (int j = 0; j < number_of_dimensions; j++) {
                type_conf_list[i][j] = Integer.valueOf(items_list.get(i + 3)[j]);
            }
        }

        int sum = 0;
        for (int i = 0; i < ratio_list.length; i++) {
            sum = sum + ratio_list[i];
        }

        double[] percentage_list = new double[number_of_type];
        for (int i = 0; i < ratio_list.length; i++) {
            percentage_list[i] = (ratio_list[i] * 1.0) / sum;
        }

        // print related information
        System.out.println("number of type: " + number_of_type);
        System.out.println("number of dimension: " + number_of_dimensions);
        System.out.print("ratio_list: ");
        for (int i = 0; i < ratio_list.length; i++) {
            System.out.print(ratio_list[i] + ",");
        }
        System.out.println();
        System.out.println("type_conf: ");
        for (int i = 0; i < type_conf_list.length; i++) {
            for (int j = 0; j < type_conf_list[i].length; j++) {
                System.out.print(type_conf_list[i][j] + ",");
            }
            System.out.println();
        }
        for (int i = 0; i < percentage_list.length; i++) {
            System.out.println(Math.round(percentage_list[i] * 100) / 100.0);
        }

        EnumeratedIntegerDistribution typeDistribution = new EnumeratedIntegerDistribution(type_name_list, percentage_list);

        int parallel_submit_number = number_of_type;
        int vm_id = 1;
        int start_time = 0;
        while (true) {
            for (int i = 0; i < parallel_submit_number; i++) {
                System.out.println("Generating VM " + vm_id);
                String[] outitem = new String[4 + number_of_dimensions];
                int runtime = runtimeDistribution.sample();
                int end_time = start_time + runtime;
                int type_name = typeDistribution.sample();

                // vm_id+vm_type+start_time+end_time+cpu+mem+disk
                outitem[0] = String.valueOf(vm_id);
                outitem[1] = String.valueOf(type_name);
                outitem[2] = String.valueOf(start_time);
                outitem[3] = String.valueOf(end_time);
                for (int j = 0; j < number_of_dimensions; j++) {
                    outitem[4 + j] = String.valueOf(type_conf_list[type_name][j]);
                }
                vminfo_writer.writeNext(outitem);

                System.out.println("Generating Events");
                // generate event information: time+vm_id+type, type: 1 is start, 2 is end
                // adding start event
                if (!treeMap.containsKey(start_time)) {
                    String[] event = new String[3];
                    event[0] = String.valueOf(start_time);//time
                    event[1] = String.valueOf(vm_id);//vm_id
                    event[2] = "1";//type
                    ArrayList<String[]> strList = new ArrayList<String[]>();
                    strList.add(event);
                    treeMap.put(start_time, strList);

                } else {
                    String[] event = new String[3];
                    event[0] = String.valueOf(start_time);//time
                    event[1] = String.valueOf(vm_id);//vm_id
                    event[2] = "1";//type
                    treeMap.get(start_time).add(event);
                }

                //adding end event
                if (!treeMap.containsKey(end_time)) {
                    String[] event = new String[3];
                    event[0] = String.valueOf(end_time);//time
                    event[1] = String.valueOf(vm_id);//vm_id
                    event[2] = "2";//type
                    ArrayList<String[]> strList = new ArrayList<String[]>();
                    strList.add(event);
                    treeMap.put(end_time, strList);

                } else {
                    String[] event = new String[3];
                    event[0] = String.valueOf(end_time);//time
                    event[1] = String.valueOf(vm_id);//vm_id
                    event[2] = "2";//type
                    treeMap.get(end_time).add(event);
                }

                vm_id++;
            }

            start_time = (int) (start_time + Math.round(starttimeDistribution.sample()));
            if (start_time >= 2880) {
                break;
            }
        }

        // generate event information
        Iterator iterator = treeMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry ent = (Map.Entry) iterator.next();
            System.out.println(ent.getKey());
            ArrayList<String[]> event_list = (ArrayList<String[]>) ent.getValue();
            for (String[] strs : event_list) {
                eventinfo_writer.writeNext(strs);
            }
        }

        vminfo_writer.flush();
        vminfo_writer.close();
        eventinfo_writer.flush();
        eventinfo_writer.close();
        System.out.println("Generating Complete");
    }
}
