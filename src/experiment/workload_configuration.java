package experiment;

import au.com.bytecode.opencsv.CSVWriter;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by jiyuanshi on 6/10/15.
 */
public class workload_configuration {
    public static void main(String[] args) {
        try {
            int number_of_type = 10;
            int number_of_dimensions = 3;
            String distribution_type = "uniform";
            CSVWriter configuration_writer = new CSVWriter(new FileWriter("./workload_conf_t" + number_of_type + "_d" + number_of_dimensions + "_" + distribution_type), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER);
            UniformIntegerDistribution ratio_distribution = new UniformIntegerDistribution(1, 10);
            UniformIntegerDistribution dimensions_distribution = new UniformIntegerDistribution(1, 10);

            String[] strings_number_of_type = {String.valueOf(number_of_type)};
            configuration_writer.writeNext(strings_number_of_type);
            String[] strings_number_of_dimensions = {String.valueOf(number_of_dimensions)};
            configuration_writer.writeNext(strings_number_of_dimensions);


            String[] ratio_list = new String[number_of_type];
            for (int i = 0; i < number_of_type; i++) {
                ratio_list[i] = String.valueOf(ratio_distribution.sample());
            }
            configuration_writer.writeNext(ratio_list);

            for (int i = 0; i < 10; i++) {
                String[] dimensions_list = new String[number_of_dimensions];
                for (int j = 0; j < number_of_dimensions; j++) {
                    dimensions_list[j] = String.valueOf(dimensions_distribution.sample());
                }
                configuration_writer.writeNext(dimensions_list);
            }
            configuration_writer.flush();
            configuration_writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
