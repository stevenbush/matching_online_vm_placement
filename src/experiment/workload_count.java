package experiment;

import au.com.bytecode.opencsv.CSVReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.SimpleTimeZone;

/**
 * Created by jiyuanshi on 6/10/15.
 */
public class workload_count {
    public static void main(String[] args) {
        try {
            CSVReader item_reader = new CSVReader(new FileReader("./vm_info"));
            List<String[]> item_list = item_reader.readAll();

            int[][] vmtype_cal_info = new int[60][5];

            for (int i = 0; i < item_list.size(); i++) {
                System.out.println(item_list.get(i)[0] + "," + item_list.get(i)[1] + "," + item_list.get(i)[2] + "," + item_list.get(i)[3]);

                int vm_type = Integer.valueOf(item_list.get(i)[1]);
                int start_time = Integer.valueOf(item_list.get(i)[2]);
                int end_time = Integer.valueOf(item_list.get(i)[3]);

                for (int j = start_time; j <= end_time; j++) {
                    int hour_indext = j / 60;
                    vmtype_cal_info[hour_indext][vm_type - 1]++;
                }
            }

            // print the calculation information
            System.out.println("print the number of different vm type in each hour:");
            for (int i = 0; i < 60; i++) {
                System.out.println(i + 1 + ": " + vmtype_cal_info[i][0] + "," + vmtype_cal_info[i][1] + "," + vmtype_cal_info[i][2] + "," + vmtype_cal_info[i][3] + "," + vmtype_cal_info[i][4]);
            }

            System.out.println("print the ratio of different vm type in each hour");

            double[][] vmtype_ratio_info = new double[60][5];
            for (int i = 0; i < 60; i++) {
                int sum = vmtype_cal_info[i][0] + vmtype_cal_info[i][1] + vmtype_cal_info[i][2] + vmtype_cal_info[i][3] + vmtype_cal_info[i][4];
                vmtype_ratio_info[i][0] = Math.round((vmtype_cal_info[i][0] * 100.0) / sum) / 100.0;
                vmtype_ratio_info[i][1] = Math.round((vmtype_cal_info[i][1] * 100.0) / sum) / 100.0;
                ;
                vmtype_ratio_info[i][2] = Math.round((vmtype_cal_info[i][2] * 100.0) / sum) / 100.0;
                ;
                vmtype_ratio_info[i][3] = Math.round((vmtype_cal_info[i][3] * 100.0) / sum) / 100.0;
                ;
                vmtype_ratio_info[i][4] = Math.round((vmtype_cal_info[i][4] * 100.0) / sum) / 100.0;
                ;

                System.out.println(i + 1 + ": " + vmtype_ratio_info[i][0] + "," + vmtype_ratio_info[i][1] + "," + vmtype_ratio_info[i][2] + "," + vmtype_ratio_info[i][3] + "," + vmtype_ratio_info[i][4]);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
