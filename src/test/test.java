package test;

import au.com.bytecode.opencsv.CSVReader;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 * Created by jiyuanshi on 6/10/15.
 */
public class test {
    public static void main(String[] args) throws IOException {
        CSVReader event_reader = new CSVReader(new FileReader("workload_conf_t10_d3_uniform"));
        List<String[]> items_list = event_reader.readAll();

        for (int i = 0; i < items_list.size(); i++) {
            for (int j = 0; j < items_list.get(i).length; j++) {
                //System.out.println(items_list.get(i)[j]);
                System.out.print(items_list.get(i)[j] + ",");
            }
            System.out.println();
        }

        event_reader.close();
    }
}
