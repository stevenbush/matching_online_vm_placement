package test;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;
import org.apache.commons.math3.distribution.ExponentialDistribution;

/**
 * Created by jiyuanshi on 6/10/15.
 */
public class distribution_test {
    public static void main(String[] args) {
        double[] ratio_list = {0.25, 0.75};
        int[] type_list = {1, 2};
        EnumeratedIntegerDistribution distribution = new EnumeratedIntegerDistribution(type_list, ratio_list);

        for (int i = 0; i < 10; i++) {
            System.out.println(distribution.sample());
        }

    }
}
